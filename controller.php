
 const PAGINATION_ROW_PER_PAGE = 5;
 public function index()
    {
        $rowsPerPage = self::PAGINATION_ROW_PER_PAGE;

        return view('reports.index',compact('rowsPerPage'));
    }

public function getReport()
    {

        $paginatePerPage = !is_null(\request('rows_per_page')) ? \request('rows_per_page') : self::PAGINATION_ROW_PER_PAGE;
        $filterableColumns = \request('filterable_columns');

        $query = Report::latest();
        /*Filtering by column*/
        if(!is_null($filterableColumns)){
            $columns = explode(';', $filterableColumns);
            foreach ($columns as $column){
                $columnName = explode('|', $column);
                if(isset($columnName[0]) && isset($columnName[1]) ){
                    if($columnName[1]) {
                        $query = $query->where($columnName[0], $columnName[1]);
                    }
                }
            }
        }
        //////////////////////////////////////////////////////////

        $reports = $query->paginate($paginatePerPage);
        $reportsArray = $reports->toArray();

        return response()->json([
            'status'=>'ok',
            'data'=>$reports,
            'pages' => $this->getPages($reportsArray['current_page'], $reportsArray['last_page'], $reportsArray['total']),
            'sl' => !is_null(\request()->page) ? (\request()->page -1 ) * $paginatePerPage : 0

        ]);
    }
    private function getPages($currentPage, $lastPage, $totalPages)
    {
        $startPage = ($currentPage < 5)? 1 : $currentPage - 4;
        $endPage = 8 + $startPage;
        $endPage = ($totalPages < $endPage) ? $totalPages : $endPage;
        $diff = $startPage - $endPage + 8;
        $startPage -= ($startPage - $diff > 0) ? $diff : 0;
        $pages = [];

        if ($startPage > 1) {
            $pages[] = '...';
        }

        for($i=$startPage; $i<=$endPage && $i<=$lastPage; $i++){
            $pages[] = $i;
        }

        if ($currentPage < $lastPage){
            $pages[] = '...';
        }

        return $pages;
    }