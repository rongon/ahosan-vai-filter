<div class="row">
    <div class="col-sm-12 col-md-3">
        {!! Form::select('directorate',
        [
        'HI'=>'HI'
        ], null, [
        'class'=>'form-control',
        'id'=>'directorate',
        'placeholder'=>'অধিদপ্তর',
        ]) !!}
    </div>

</div>
<div class="row mt-2">
    <div class="col-sm-12 col-md-6">
        <button onclick="clearAllFilters()" class="btn btn-sm btn-danger">
            X {{ __('Clear Filter Options') }}
        </button>
    </div>

    <div class="col-sm-12  col-md-6  ">
        <div class="form-group form-inline  float-right">
            <label for="numberOfRowsPerPage"> {{ __('Item per page') }}</label>&nbsp;
            <select class="form-control" id="numberOfRowsPerPage">
                <option {{ $rowsPerPage=='5'? 'selected' : '' }}>5</option>
                <option {{ $rowsPerPage=='10'? 'selected' : '' }}>10</option>
                <option {{ $rowsPerPage=='20'? 'selected' : '' }}>20</option>
                <option {{ $rowsPerPage=='30'? 'selected' : '' }}>30</option>
                <option {{ $rowsPerPage=='40'? 'selected' : '' }}>40</option>
                <option {{ $rowsPerPage=='50'? 'selected' : '' }}>50</option>
                <option {{ $rowsPerPage=='100'? 'selected' : '' }}>100</option>
            </select>
        </div>
    </div>
</div>

<table>
    <thead></thead>
    <tbody id="tbodyItems"></tbody>
</table>

<nav aria-label="Page navigation" class="text-uppercase" id="ponditPagination">
    <ul class="pagination float-right" id="pagesList"></ul>
    <div>Page: <span class="badge badge-primary text-white current-page-number">0</span> of
        <span class="badge badge-primary text-white total-page-number">0</span> Records: <span class="badge badge-primary text-white total-record-number">0</span></div>
</nav>

<script>
    const filterableColumns = ['audit_report_name'];

    /*Clear filter options*/
    function clearAllFilters(){
        filterableColumns.forEach(function (column) {
            document.querySelector('#'+column).value = '';
        });
        index()
    }

    window.onload = function () {

        /*Number of rows onchange*/
        let numberOfRowsPerPage = document.querySelector('#numberOfRowsPerPage');
        numberOfRowsPerPage.addEventListener('change', function () {
            index();
        });

        /*Filterable columns onchange*/
        filterableColumns.forEach(function (column) {
            document.getElementById(column).addEventListener('change', function () {
                index();
            })
        })
        index();
    }

    function index(page_url = null) {
        let numberOfRowsPerPageInput = document.querySelector('#numberOfRowsPerPage');
        let rowsPerPage = numberOfRowsPerPageInput.options[numberOfRowsPerPageInput.selectedIndex].value
        // let keywordInput = document.getElementById('keyword');
        // let searchKeyWord = keywordInput.value;
        let queryString = '';
        // queryString += searchKeyWord != ''? 'keyword='+searchKeyWord : '';
        queryString += '&rows_per_page='+rowsPerPage;
        let filterColumns = '';
        filterableColumns.forEach(function (column) {
            let val = document.querySelector('#'+column).value;
            filterColumns += column+'|'+val+';';
        });
        queryString += filterColumns.length>0 ? '&filterable_columns='+ filterColumns : '';
        page_url = page_url || `get-reports?${queryString}`;
        axios.get(page_url)
            .then(res => {
                let numberOfRowsPerPageInput = document.querySelector('#numberOfRowsPerPage');
                let rowsPerPage = numberOfRowsPerPageInput.options[numberOfRowsPerPageInput.selectedIndex].value
                let parentElement = document.querySelector('#tbodyItems');
                parentElement.innerHTML = '';
                let items = res.data.data;
                let sl = res.data.sl + 1;
                items.data.forEach(item => {
                    createRowElement(item, parentElement, sl++)
                });
                // // /*Required arguments for pagination*/
                let searchKeyWord = '';
                return {
                    parent_element: parentElement,
                    url: `get-reports`,
                    current_page: items.current_page,
                    last_page: items.last_page,
                    total_rows: items.total,
                    keyword: searchKeyWord,
                    row_per_page: rowsPerPage,
                    pages: res.data.pages,
                    query_string: queryString
                };
            })
            .then(data => {
                makePagination(data);
            })
            .catch(err => console.log(err))
    }


    function createRowElement(data, parentElement, sl) {

        const TR = document.createElement('tr');

        const slNo = document.createElement('td');
        slNo.innerHTML = sl;
        TR.appendChild(slNo);

        const auditReportName = document.createElement('td');
        auditReportName.innerHTML = data.audit_report_name;
        TR.appendChild(auditReportName);

        const orthoBochor = document.createElement('td');
        orthoBochor.innerHTML = data.ortho_bochor;
        TR.appendChild(orthoBochor);

        const yearFrom = document.createElement('td');
        yearFrom.innerHTML = data.year_from;
        TR.appendChild(yearFrom);

        const yearTo = document.createElement('td');
        yearTo.innerHTML = data.year_to;
        TR.appendChild(yearTo);

        const isAlochito = document.createElement('td');

        isAlochito.innerHTML = data.is_alochito;
        TR.appendChild(isAlochito);



        const tdActions = document.createElement('td');
        tdActions.className = 'text-center d-flex';

        // const showBtn = document.createElement('a');
        // showBtn.className = 'btn  btn-sm btn-info';
        // showBtn.innerHTML = '<i class="fa fa-eye" aria-hidden="true"></i>'
        // showBtn.setAttribute("title", "Show Details");
        // showBtn.setAttribute("href", `apottis/${data.id}`);
        // showBtn.setAttribute("target", '_blank');
        // tdActions.appendChild(showBtn);

        const editBtn = document.createElement('a');
        editBtn.innerHTML = '<button class="btn btn-rounded btn-warning btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></button>'
        editBtn.setAttribute("title", "Edit ");
        editBtn.setAttribute("href", `report/${data.id}/edit`);
        // editBtn.setAttribute("target", '_blank');
        tdActions.appendChild(editBtn);

        let csrfToken = '{{ Session::token() }}';
        let formElements = `
                                <input type="hidden" name="_token" value="${csrfToken}" />
                                <button class="btn btn-rounded  btn-danger btn-sm" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                <input type="hidden" name="_method" value="DELETE">
                            `

        const deleteForm = document.createElement('form');
        deleteForm.innerHTML = formElements;
        deleteForm.setAttribute("method", "post");
        deleteForm.setAttribute("action", `report/${data.id}`);
        tdActions.appendChild(deleteForm);

        TR.appendChild(tdActions);
        parentElement.appendChild(TR);
    }
</script>
